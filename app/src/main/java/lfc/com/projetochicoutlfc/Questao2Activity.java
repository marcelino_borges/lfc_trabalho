package lfc.com.projetochicoutlfc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Questao2Activity extends AppCompatActivity {

    private String[] palavras = new String[] {"a12", "12d", "2dr", "1dr=", "rvv", "rav12d",
                                                "var", "=", "d", "12"};

    private EditText editText;

    private Button btnVerificar, btnProximo, btnVoltar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questao2);

        btnVerificar = findViewById(R.id.btnVerificar2);
        btnProximo = findViewById(R.id.btnProximo2);
        editText = findViewById(R.id.editTextQuestao2);
        btnVoltar = findViewById(R.id.btnVoltar2);

        btnVerificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Pegando o texto digitado
                String inputText = editText.getText().toString();

                //Bool de controle para saber se é válido no final
                boolean isValid = true;

                for(String palavra : palavras) {
                    isValid = true;
                    if(inputText.equalsIgnoreCase(palavra)) {
                        break;
                    } else {
                        isValid = false;
                    }
                }

                if(!isValid) {
                    exibirToast("Palavra \"" + inputText + "\" não permitida.");
                } else {
                    exibirToast("Palavra permitida!");
                }
            }
        });

        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proximaActivity();
            }
        });
    }

    private void exibirToast(String texto) {
        Toast.makeText(this,texto,Toast.LENGTH_LONG).show();
    }

    private void proximaActivity() {
        Intent intent = new Intent(this, Questao3Activity.class);
        startActivity(intent);
    }
}
