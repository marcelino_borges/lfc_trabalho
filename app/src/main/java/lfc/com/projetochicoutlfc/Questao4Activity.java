package lfc.com.projetochicoutlfc;

import android.content.Intent;
import android.media.MediaScannerConnection;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Questao4Activity extends AppCompatActivity {

    private String[] pontuacaoParaRetirar = new String[] {",",".",";",":","!","?"};

    private EditText editText;

    private Button btnVerificar, btnProximo, btnVoltar, btnAddAoArquivo, btnLimparArquivo;

    private static final String NOME_ARQUIVO = "ChicoLang.txt";

    private final String TEXTO_OBJETIVO = "var d = 12";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questao4);

        btnVerificar = findViewById(R.id.btnVerificarQ4);
        btnProximo = findViewById(R.id.btnProximo4);
        btnAddAoArquivo = findViewById(R.id.btnAddAoArquivo4);
        btnLimparArquivo = findViewById(R.id.btnLimparArquivo4);
        btnVoltar = findViewById(R.id.btnVoltar4);
        editText = findViewById(R.id.editTextQuestao4);

        btnAddAoArquivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTextoAoArquivo(editText.getText().toString());
            }
        });

        btnVerificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (arquivoExiste()) {
                    if(acharTextoNoArquivo(TEXTO_OBJETIVO)) {
                        startActivity(new Intent(Questao4Activity.this, ParabensActivity.class));
                    } else {
                        exibirToast("Bang! TextoNãoLocalizadoException!");
                    }

                    //Caso o arquivo não exista...
                } else {
                    exibirToast("Arquivo ainda não existe. Favor informe um texto para criar um.");
                }
            }
        });

        btnLimparArquivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limparArquivo();
            }
        });

        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proximaActivity();
            }
        });
    }

    private boolean arquivoExiste() {
        File arquivo = new File(this.getExternalFilesDir(null), NOME_ARQUIVO);
        return arquivo.exists();
    }

    private void addTextoAoArquivo(String text) {
        try {
            // Creates a file in the primary external storage space of the
            // current application.
            // If the file does not exists, it is created.
            File arquivo = new File(this.getExternalFilesDir(null), NOME_ARQUIVO);
            if (!arquivo.exists())
                arquivo.createNewFile();

            // Adds a line to the file
            BufferedWriter writer = new BufferedWriter(new FileWriter(arquivo, true /*append*/));

            writer.write(text);

            writer.close();
            // Refresh the data so it can seen when the device is plugged in a
            // computer. You may have to unplug and replug the device to see the
            // latest changes. This is not necessary if the user should not modify
            // the files.
            MediaScannerConnection.scanFile(this,
                    new String[]{arquivo.toString()},
                    null,
                    null);
            exibirToast("Texto adicionado ao " + NOME_ARQUIVO + " arquivo com sucesso!");
        } catch (IOException e) {
            Log.e("ReadWriteFile", "Não foi possível escrever no " + NOME_ARQUIVO + ".");
            exibirToast("Não foi possível escrever no arquivo " + NOME_ARQUIVO + ".");
        }
    }

    public boolean acharTextoNoArquivo(String texto){
        // Gets the file from the primary external storage space of the
        // current application.
        File arquivo = new File(this.getExternalFilesDir(null), NOME_ARQUIVO);
        if (arquivo.exists()) {
            StringBuilder stringBuilder = new StringBuilder();
            // Reads the data from the file
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(arquivo));
                String line;

                while ((line = reader.readLine()) != null) {
                    if(line.equals(texto)) {
                        return true;
                    }
                }
                reader.close();

            } catch (Exception e) {
                Log.e("ReadWriteFile", "Não foi possível ler o arquivo " + NOME_ARQUIVO);
                exibirToast("Não foi possível ler o arquivo " + NOME_ARQUIVO);
            }
        }

        return false;
    }

    private void limparArquivo() {
        File arquivo = new File(this.getExternalFilesDir(null), NOME_ARQUIVO);

        //Se o arquivo existir
        if(arquivo.exists()) {
            //Deleta ele
            if(arquivo.delete()) {
                exibirToast("Arquivo " + NOME_ARQUIVO + " deletado com sucesso");
            }
        } else {
            exibirToast("Arquivo " + NOME_ARQUIVO + " ainda não existe.");
        }

    }

    private void exibirToast(String texto) {
        Toast.makeText(this,texto,Toast.LENGTH_LONG).show();
    }

    private String ArrayToString(ArrayList<String> array) {
        String string = "";

        for(String item : array) {
            int index = array.indexOf(item);
            if (index == 0) {
                string += item;
            } else if(index == array.size() - 1) {
                string += " e " + item;
            } else {
                string += ", " + item;
            }
        }

        return string;
    }

    private void proximaActivity() {
        Intent intent = new Intent(this, Questao4Activity.class);
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        limparArquivo();
    }
}
