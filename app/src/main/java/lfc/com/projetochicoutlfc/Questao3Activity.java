package lfc.com.projetochicoutlfc;

import android.content.Intent;
import android.media.MediaScannerConnection;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Questao3Activity extends AppCompatActivity {

    private EditText editText;

    private Button btnVerificar, btnProximo, btnVoltar, btnAddAoArquivo, btnLimparArquivo;

    private String[] palavras = new String[] {"a12", "12d", "2dr", "1dr=", "rvv", "rav12d",
            "var", "=", "d", "12"};
    private String[] pontuacaoParaRetirar = new String[] {",",".",";",":","!","?"};
    private static final String NOME_ARQUIVO = "ChicoLang.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questao3);

        btnVerificar = findViewById(R.id.btnVerificarQ3);
        btnProximo = findViewById(R.id.btnProximo3);
        btnAddAoArquivo = findViewById(R.id.btnAddAoArquivo);
        btnLimparArquivo = findViewById(R.id.btnLimparArquivo);
        btnVoltar = findViewById(R.id.btnVoltar3);
        editText = findViewById(R.id.editTextQuestao3);

        btnAddAoArquivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTextoAoArquivo(editText.getText().toString());
            }
        });

        btnVerificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (arquivoExiste()) {
                    String[] palavrasArquivo = lerArquivo();

                    //Lista com os caracteres nao encontrados (pra ser exibido no final)
                    ArrayList<String> palavrasNaoEncontradas = localizarPalavrasProibidas(palavrasArquivo);

                    //Se houver elementos devolvidos para a lista de palavras não encontradas,
                    // logo a palavra é inválida
                    if (palavrasNaoEncontradas.size() > 0) {
                        //Se a lista tiver mais de 1 elemento, tratar no plural
                        if (palavrasNaoEncontradas.size() > 1)
                            exibirToast("Palavra(s) \"" + ArrayToString(palavrasNaoEncontradas) + "\" não permitida(s).");
                        //Se a lista tiver apenas 1 elemento, tratar no singular
                        else
                            exibirToast("Palavra \"" + ArrayToString(palavrasNaoEncontradas) + "\" não permitida.");
                    //Se a lista estiver vazia, o código encontrou todas as palavras
                    } else {
                        exibirToast("Palavra(s) permitida(s)!");
                    }

                //Caso o arquivo não exista...
                } else {
                    exibirToast("Arquivo ainda não existe. Favor informe um texto para criar um.");
                }
            }
        });

        btnLimparArquivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limparArquivo();
            }
        });

        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proximaActivity();
            }
        });
    }

    private ArrayList<String> localizarPalavrasProibidas(String[] palavrasArquivo) {
        ArrayList<String> palavrasNaoEncontradasTemp = new ArrayList<>();
        boolean palavraEncontrada;

        for (int i = 0; i < palavrasArquivo.length; i++) {
            //Bool de controle para saber se a letra em análise foi encontrada no fim da análise
            palavraEncontrada = false;

            for (int j = 0; j < palavras.length; j++) {
                //Se a palavra analisada for encontrada no alfabeto, seta a bool para true
                if (palavrasArquivo[i].equalsIgnoreCase(palavras[j])) {
                    palavraEncontrada = true;
                    break;
                }
            }

            //Se a palavra palavrasArquivo[i] não for encontrada no vocabulário
            if (!palavraEncontrada) {
                //Verifica se ela já não está na lista
                if (!palavrasNaoEncontradasTemp.contains(palavrasArquivo[i])) {
                    //Se não, add ela na lista
                    palavrasNaoEncontradasTemp.add(palavrasArquivo[i]);
                }
            }
        }
        return palavrasNaoEncontradasTemp;
    }

    private boolean arquivoExiste() {
        File arquivo = new File(this.getExternalFilesDir(null), NOME_ARQUIVO);
        return arquivo.exists();
    }

    private void addTextoAoArquivo(String text) {
        try {
            // Creates a file in the primary external storage space of the
            // current application.
            // If the file does not exists, it is created.
            File arquivo = new File(this.getExternalFilesDir(null), NOME_ARQUIVO);
            if (!arquivo.exists())
                arquivo.createNewFile();

            // Adds a line to the file
            BufferedWriter writer = new BufferedWriter(new FileWriter(arquivo, true /*append*/));

            writer.write(text);

            writer.close();
            // Refresh the data so it can seen when the device is plugged in a
            // computer. You may have to unplug and replug the device to see the
            // latest changes. This is not necessary if the user should not modify
            // the files.
            MediaScannerConnection.scanFile(this,
                    new String[]{arquivo.toString()},
                    null,
                    null);
            exibirToast("Texto adicionado ao " + NOME_ARQUIVO + " arquivo com sucesso!");
        } catch (IOException e) {
            Log.e("ReadWriteFile", "Não foi possível escrever no " + NOME_ARQUIVO + ".");
            exibirToast("Não foi possível escrever no arquivo " + NOME_ARQUIVO + ".");
        }
    }

    public String[] lerArquivo(){
        String textFromFile = "";
        // Gets the file from the primary external storage space of the
        // current application.
        File arquivo = new File(this.getExternalFilesDir(null), NOME_ARQUIVO);
        if (arquivo != null) {
            StringBuilder stringBuilder = new StringBuilder();
            // Reads the data from the file
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(arquivo));
                String line;

                while ((line = reader.readLine()) != null) {
                    textFromFile += line.toString();
                    textFromFile += " ";
                }
                reader.close();

            } catch (Exception e) {
                Log.e("ReadWriteFile", "Não foi possível ler o arquivo " + NOME_ARQUIVO);
                exibirToast("Não foi possível ler o arquivo " + NOME_ARQUIVO);
            }
        }
        String tempString;

        if(textFromFile.length() > 0) {
            for (String caractere : pontuacaoParaRetirar) {
                //Retirando a pontuação do texto

                tempString = textFromFile.replace(caractere, "");
                textFromFile = tempString;
            }
        }
        return textFromFile.split(" ");
    }

    private void limparArquivo() {
        File arquivo = new File(this.getExternalFilesDir(null), NOME_ARQUIVO);

        //Se o arquivo existir
        if(arquivo.exists()) {
            //Deleta ele
            if(arquivo.delete()) {
                exibirToast("Arquivo " + NOME_ARQUIVO + " deletado com sucesso");
            }
        } else {
            exibirToast("Arquivo " + NOME_ARQUIVO + " ainda não existe.");
        }

    }

    private void exibirToast(String texto) {
        Toast.makeText(this,texto,Toast.LENGTH_LONG).show();
    }

    private String ArrayToString(ArrayList<String> array) {
        String string = "";

        for(String item : array) {
            int index = array.indexOf(item);
            if (index == 0) {
                string += item;
            } else if(index == array.size() - 1) {
                string += " e " + item;
            } else {
                string += ", " + item;
            }
        }

        return string;
    }

    private void proximaActivity() {
        Intent intent = new Intent(this, Questao4Activity.class);
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        limparArquivo();
    }
}
