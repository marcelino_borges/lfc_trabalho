package lfc.com.projetochicoutlfc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private char[] alfabeto = new char[] {'a', '1', '2', 'd', 'r', 'v', '='};

    private EditText editText;

    private Button btnVerificar, btnProximo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnVerificar = findViewById(R.id.btnVerificar1);
        btnProximo = findViewById(R.id.btnProximo1);
        editText = findViewById(R.id.editTextQuestao1);

        btnVerificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Pegando o texto digitado
                String input = editText.getText().toString();
                //Convertendo o texto digitado para um vetor de char
                char[] letras = input.toCharArray();

                //Lista com os caracteres nao encontrados (pra ser exibido no final)
                ArrayList<String> charsNaoEncontrados = new ArrayList<>();

                //Bool de controle para saber se é válido no final
                boolean isValid = true;

                for(int i = 0; i < letras.length; i++) {
                    //Bool de controle para saber se a letra em análise foi encontrada no fim da análise
                    boolean letterFound = false;

                    for(int j = 0; j < alfabeto.length; j++) {
                        //Se a letra analisada for encontrada no alfabeto, seta a bool para true
                        if(letras[i] == alfabeto[j]) {
                            letterFound = true;
                            break;
                        }
                    }
                    //Se a letra não foi encontrada
                    if(!letterFound) {
                        //Só adiciona à lista caracteres que não estiverem na lista
                        if(!charsNaoEncontrados.contains(String.valueOf(letras[i]))) {
                            charsNaoEncontrados.add(String.valueOf(letras[i]));
                        }
                        isValid = false;
                    }
                }

                if(!isValid) {
                    if(charsNaoEncontrados.size() > 1)
                        exibirToast("Caracteres \"" + ArrayToString(charsNaoEncontrados) + "\" não permitidos.");
                    else
                        exibirToast("Caractere \"" + ArrayToString(charsNaoEncontrados) + "\" não permitido.");
                } else {
                    exibirToast("Caracteres permitidos!");
                }

            }
        });

        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proximaActivity();
            }
        });
    }

    private void exibirToast(String texto) {
        Toast.makeText(this,texto,Toast.LENGTH_LONG).show();
    }

    private String ArrayToString(ArrayList<String> array) {
        String string = "";

        for(String item : array) {
            int index = array.indexOf(item);
            if (index == 0) {
                string += item;
            } else if(index == array.size() - 1) {
                string += " e " + item;
            } else {
                string += ", " + item;
            }
        }

        return string;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void proximaActivity() {
        Intent intent = new Intent(this, Questao2Activity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
